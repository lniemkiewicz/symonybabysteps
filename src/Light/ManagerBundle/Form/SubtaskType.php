<?php

namespace Light\ManagerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SubtaskType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('specyfication')
            ->add('startDate')
            ->add('endDate')
            ->add('finished')
            ->add('department')
           
           
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Light\ManagerBundle\Entity\Subtask'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'light_managerbundle_subtask';
    }
}
