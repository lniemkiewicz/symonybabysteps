<?php

namespace Light\ManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Subtask
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Light\ManagerBundle\Entity\SubtaskRepository")
 */
class Subtask
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    /**
     * 
     * @ORM\ManyToOne(targetEntity="Department")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id", nullable=false)
     */
    private $department;
    
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Task", inversedBy="subtasks")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", nullable=false)
     */
    private $task;
    
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="specyfication", type="text")
     */
    private $specyfication;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime")
     */
    private $endDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="finished", type="boolean")
     */
    private $finished;


    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set specyfication
     *
     * @param string $specyfication
     * @return Subtask
     */
    public function setSpecyfication($specyfication)
    {
        $this->specyfication = $specyfication;

        return $this;
    }

    /**
     * Get specyfication
     *
     * @return string 
     */
    public function getSpecyfication()
    {
        return $this->specyfication;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return Subtask
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Subtask
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set finished
     *
     * @param boolean $finished
     * @return Subtask
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;

        return $this;
    }

    /**
     * Get finished
     *
     * @return boolean 
     */
    public function getFinished()
    {
        return $this->finished;
    }

    /**
     * Set department
     *
     * @param \Light\ManagerBundle\Entity\Department $department
     * @return Subtask
     */
    public function setDepartment(\Light\ManagerBundle\Entity\Department $department)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \Light\ManagerBundle\Entity\Department 
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set task
     *
     * @param \Light\ManagerBundle\Entity\Task $task
     * @return Subtask
     */
    public function setTask(\Light\ManagerBundle\Entity\Task $task)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return \Light\ManagerBundle\Entity\Task 
     */
    public function getTask()
    {
        return $this->task;
    }
}
