<?php

namespace Light\ManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Task
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Light\ManagerBundle\Entity\TaskRepository")
 */
class Task
{
    
    public function __construct() {
        $this->subtasks=new ArrayCollection();
    }
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Client");
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     */
    protected $client;
    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime")
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="specification", type="text")
     */
    private $specification;

    /**
     * @var boolean
     *
     * @ORM\Column(name="finished", type="boolean", nullable=true)
     */
    private $finished;

    /**
     * @var boolean
     *
     * @ORM\Column(
     *  name="invoiced", 
     *  type="boolean", 
     *  nullable = true
     * )
     */
    private $invoiced;

    
    /**
     *
     * @ORM\OneToMany(targetEntity="Subtask", mappedBy="task", cascade={"persist"})
     */
    protected $subtasks;
    
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return Task
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Task
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set specification
     *
     * @param string $specification
     * @return Task
     */
    public function setSpecification($specification)
    {
        $this->specification = $specification;

        return $this;
    }

    /**
     * Get specification
     *
     * @return string 
     */
    public function getSpecification()
    {
        return $this->specification;
    }

    /**
     * Set finished
     *
     * @param boolean $finished
     * @return Task
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;

        return $this;
    }

    /**
     * Get finished
     *
     * @return boolean 
     */
    public function getFinished()
    {
        return $this->finished;
    }

    /**
     * Set invoiced
     *
     * @param boolean $invoiced
     * @return Task
     */
    public function setInvoiced($invoiced)
    {
        $this->invoiced = $invoiced;

        return $this;
    }

    /**
     * Get invoiced
     *
     * @return boolean 
     */
    public function getInvoiced()
    {
        return $this->invoiced;
    }

  


    /**
     * Set client
     *
     * @param \Light\ManagerBundle\Entity\Client $client
     * @return Task
     */
    public function setClient(\Light\ManagerBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Light\ManagerBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }
    
    public function __toString() {
        return "id: ".$this->getId();
    }


    /**
     * Add subtasks
     *
     * @param \Light\ManagerBundle\Entity\Subtask $subtasks
     * @return Task
     */
    public function addSubtask(\Light\ManagerBundle\Entity\Subtask $subtasks)
    {
        $this->subtasks[] = $subtasks;

        return $this;
    }

    /**
     * Remove subtasks
     *
     * @param \Light\ManagerBundle\Entity\Subtask $subtasks
     */
    public function removeSubtask(\Light\ManagerBundle\Entity\Subtask $subtasks)
    {
        $this->subtasks->removeElement($subtasks);
    }

    /**
     * Get subtasks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSubtasks()
    {
        return $this->subtasks;
    }
}
